class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?


  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :password_confirmation,
                                  :avatar, :avatar_cache,:about])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :remember_me])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :email, :password, :password_confirmation,
             :current_password, :avatar, :avatar_cache, :remove_avatar,:about])
  end
end
