class User < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  mount_uploader :avatar, AvatarUploader

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Relationships
  has_many :posts
  has_many :following, :class_name => 'Follower', :foreign_key => 'current_id'

  # User Avatar Validation
  validates :avatar, presence: true
  validates_integrity_of  :avatar
  validates_processing_of :avatar

  scope :filter_all, -> (page) {User.all}

  private

  def avatar_size_validation
    errors[:avatar] << "should be less than 500KB" if avatar.size > 0.5.megabytes
  end

end
