# -*- encoding: utf-8 -*-

require 'spec_helper'
require './uploaders/avatar_uploader'

describe AvatarUploader do

  # TODO: auto-generated
  describe '#store_dir' do
    it 'works' do
      avatar_uploader = AvatarUploader.new
      result = avatar_uploader.store_dir
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#extension_white_list' do
    it 'works' do
      avatar_uploader = AvatarUploader.new
      result = avatar_uploader.extension_white_list
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#default_url' do
    it 'works' do
      avatar_uploader = AvatarUploader.new
      *args = double('*args')
      result = avatar_uploader.default_url(*args)
      expect(result).not_to be_nil
    end
  end

end
