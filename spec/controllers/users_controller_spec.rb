# -*- encoding: utf-8 -*-

require 'spec_helper'
require './controllers/users_controller'

describe UsersController do

  # TODO: auto-generated
  describe '#index' do
    it 'works' do
      users_controller = UsersController.new
      result = users_controller.index
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#search' do
    it 'works' do
      users_controller = UsersController.new
      result = users_controller.search
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#show' do
    it 'works' do
      users_controller = UsersController.new
      result = users_controller.show
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#showfollowing' do
    it 'works' do
      users_controller = UsersController.new
      result = users_controller.showfollowing
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#showfollowers' do
    it 'works' do
      users_controller = UsersController.new
      result = users_controller.showfollowers
      expect(result).not_to be_nil
    end
  end

end
