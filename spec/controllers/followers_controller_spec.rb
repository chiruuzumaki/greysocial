# -*- encoding: utf-8 -*-

require 'spec_helper'
require './controllers/followers_controller'

describe FollowersController do

  # TODO: auto-generated
  describe '#follow' do
    it 'works' do
      followers_controller = FollowersController.new
      result = followers_controller.follow
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#unfollow' do
    it 'works' do
      followers_controller = FollowersController.new
      result = followers_controller.unfollow
      expect(result).not_to be_nil
    end
  end

end
