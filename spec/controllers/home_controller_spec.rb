# -*- encoding: utf-8 -*-

require 'spec_helper'
require './controllers/home_controller'

describe HomeController do

  # TODO: auto-generated
  describe '#resource_name' do
    it 'works' do
      home_controller = HomeController.new
      result = home_controller.resource_name
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#resource' do
    it 'works' do
      home_controller = HomeController.new
      result = home_controller.resource
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#resource_class' do
    it 'works' do
      home_controller = HomeController.new
      result = home_controller.resource_class
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#devise_mapping' do
    it 'works' do
      home_controller = HomeController.new
      result = home_controller.devise_mapping
      expect(result).not_to be_nil
    end
  end

  # TODO: auto-generated
  describe '#index' do
    it 'works' do
      home_controller = HomeController.new
      result = home_controller.index
      expect(result).not_to be_nil
    end
  end

end
